import React, {
  KeyboardEvent, useEffect,
  useRef
} from 'react';
import { fetchDelete, fetchGet, fetchPost } from './http-utils';

type Item = {
  id: number,
  name: string
}
const API_URL = 'http://localhost:3001/items';

export const DemoListFetch: React.FC = () => {
  const inputEl = useRef<HTMLInputElement>(null);
  const [list, setList] = React.useState<Item[]>([]);
  const [error, setError] = React.useState<boolean>(false);

  useEffect(() => {
    async function load() {
      try {
        const response = await fetchGet(API_URL);
        setList(response)
      } catch (e) { setError(true) }
    }
    load();
  }, []);

  // delete
  const deleteItemHandler = async (itemToDelete: Item) => {
    setError(false);
    try {
      await fetchDelete(API_URL, itemToDelete.id);
      setList(state => state.filter(item => item.id !== itemToDelete.id))
    } catch (e) { setError(true) }
  };

  // add
  const addItemHandler = async (event: KeyboardEvent<HTMLInputElement>) => {
    setError(false);
    const target = inputEl.current;

    if (event.key === 'Enter' && target) {
      try {
        const response = await fetchPost(API_URL, { name: target.value });
        setList(state => [...state, response]);
      } catch (e) { setError(true) }

      target.value = '';
    }
  };


  return (
    <>
      <h3>SIMPLE LIST: #{list.length} Items</h3>
      { error && <div className="alert alert-danger">Server error</div> }

      {/*form*/}
      <input ref={inputEl} type="text" onKeyPress={addItemHandler}/>

      {/*list*/}
      {
        list.map((item: Item, index: number) => {
          return (
            <li className="list-group-item" key={index}>
              {index+1} - {item.name}
              <div className="pull-right">
                <i className="fa fa-trash" onClick={() => deleteItemHandler(item)} />
              </div>
            </li>
          )
        })
      }
    </>
  );
};
