export interface User {
  id: number;
  name: string;
  tweets: number;
}
