import React from "react";
import { Link } from 'react-router-dom';

interface ExampleMenuProps {
  message: string;
}

const ExampleMenu: React.FC<ExampleMenuProps> = () => {
  return (
    <div title="SHOW EXAMPLES">
      <nav>
        <ul className="list-group mb-2">

          <li className="list-group-item list-group-item-info">REACT FUNDAMENTALS (Simple examples)</li>

          <li className="list-group-item"><Link to="/fundamentals/hello-react">Hello React</Link></li>
          <li className="list-group-item"><Link to="/fundamentals/hello-react-components">Hello React Component</Link></li>
          <li className="list-group-item"><Link to="/fundamentals/css-class">CSS Class</Link></li>
          <li className="list-group-item"><Link to="/fundamentals/css-classnames">CSS Classnames Utility (WIP)</Link></li>
          <li className="list-group-item"><Link to="/fundamentals/inline-style">Inline Styling</Link></li>
          <li className="list-group-item"><Link to="/fundamentals/children">Props Children</Link></li>
          <li className="list-group-item"><Link to="/fundamentals/mouseevents">Mouse Events</Link></li>
          <li className="list-group-item"><Link to="/fundamentals/props-callback">Props as Callback</Link></li>
          <li className="list-group-item"><Link to="/fundamentals/useStateuseEffect">Hooks: useState & useEffect</Link></li>
        </ul>

        <ul className="list-group mb-2">
          <li className="list-group-item list-group-item-info">COMPONENTS</li>
          <li className="list-group-item"><Link to="/components/static-map-quest">Static Map (MapQuest) component</Link></li>
          <li className="list-group-item"><Link to="/components/panel">Panel Component</Link></li>
          <li className="list-group-item"><Link to="/components/toggable">Toggable component</Link></li>
          <li className="list-group-item"><Link to="/components/tabbar-generics">TabBar component (use Generics)</Link></li>
          <li className="list-group-item"><Link to="/components/tabbar-generics-custom-prop">TabBar (use Generic  & Custom Prop)</Link></li>
          <li className="list-group-item">ChartJS component & 3rd party api</li>

        </ul>



        <ul className="list-group mb-2">
          <li className="list-group-item list-group-item-info">HOOKS INTRO: useState & useEffect</li>
          <li className="list-group-item"><Link to="/hooks-intro/use-state">useState, useEffect, fetch</Link></li>
        </ul>

        <ul className="list-group mb-2">
          <li className="list-group-item list-group-item-info">LIST</li>
          <li className="list-group-item"><Link to="/list/simple">Simple List</Link></li>
          <li className="list-group-item"><Link to="/list/useState">Simple List, useState, fetch</Link></li>
        </ul>


        <ul className="list-group mb-2">
          <li className="list-group-item list-group-item-info">FORMS</li>
          <li className="list-group-item"><Link to="/forms/uncontrolled-forms">Uncontrolled forms: useRef, useEffect intro & Keyboard Events</Link></li>
          <li className="list-group-item"><Link to="/forms/controlled-forms">Controlled forms: onChange and useState</Link></li>
          <li className="list-group-item"><Link to="/forms/validations">Form Validation: multiple fields</Link></li>
        </ul>

        <ul className="list-group mb-2">
          <li className="list-group-item list-group-item-info">COMPONENT BASED APPROACH / CRUD</li>
          <li className="list-group-item"><Link to="/component-based/demo1A-usestate">Form + List: useState + useEffect</Link></li>
          <li className="list-group-item"><Link to="/component-based/demo1B-customhooks">Form + List: useState + Custom Hook</Link></li>
          <li className="list-group-item"><Link to="/component-based/demo2A-usereducer">Form + List: useReducer</Link></li>
          <li className="list-group-item"><Link to="/component-based/demo2B-customhooks">Form + List: useReducer + CustomHooks</Link></li>
        </ul>



        <ul className="list-group mb-2">
          <li className="list-group-item list-group-item-info">Reusable Hooks</li>
          <li className="list-group-item"><Link to="/reusable-hooks">Reusable Hooks Demo</Link></li>
        </ul>


        <ul className="list-group mb-2">
          <li className="list-group-item list-group-item-info">HOOKS EXAMPLES and CUSTOM HOOKS</li>
          <li className="list-group-item"><Link to="/hooks-examples/demo-use-state">useState</Link></li>
          <li className="list-group-item"><Link to="/hooks-examples/demo-use-state-custom-hook">useState Custom Hook</Link></li>
          <li className="list-group-item"><Link to="/hooks-examples/demo-use-context">useContext</Link></li>
          <li className="list-group-item"><Link to="/hooks-examples/demo-use-context-dynamic">useContext Dynamic</Link></li>
          <li className="list-group-item"><Link to="/hooks-examples/demo-use-effect">Simple useEffect</Link></li>
          <li className="list-group-item"><Link to="/hooks-examples/demo-use-effect-optimized">useEffect Optimized</Link></li>
          <li className="list-group-item"><Link to="/hooks-examples/demo-use-reducer-simple">useReducer: simple</Link></li>
          <li className="list-group-item"><Link to="/hooks-examples/demo-use-reducer-custom-hook">useReducer: custom hooks (TO DO)</Link></li>
          <li className="list-group-item"><Link to="/hooks-examples/demo-use-memo1">useMemo Simple (TO FIX)</Link></li>
          <li className="list-group-item"><Link to="/hooks-examples/demo-use-memo2">useMemo Fibonacci</Link></li>

        </ul>


        <ul className="list-group mb-2">
          <li className="list-group-item list-group-item-info">CRUD (sperimental)</li>
          <li className="list-group-item"><Link to="/crud/list-and-uncontrolled-forms">1) List & useRef</Link></li>
          <li className="list-group-item"><Link to="/crud/list-fetch">2) List & Fetch (persist - need server)</Link></li>
          <li className="list-group-item"><Link to="/crud/list-fetch-custom-hooks">3) List Custom Hooks</Link></li>
          <li className="list-group-item"><Link to="/crud/list-fetch-use-reducer">4) List useReducer</Link></li>
        </ul>

        <ul className="list-group mb-2">
          <li className="list-group-item list-group-item-info">VARIOUS</li>
          <li className="list-group-item"><Link to="/class/hello">1) Demo Class</Link></li>


          <li className="list-group-item list-group-item-info">ROUTER</li>
          <li className="list-group-item"><Link to="/products/1">Router with params</Link></li>
        </ul>
      </nav>
    </div>
  )
};

export default ExampleMenu;
