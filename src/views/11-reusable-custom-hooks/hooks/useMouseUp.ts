import { useEffect } from 'react';

export const useMouseUp = (callback: (e: Event) => void) => {
  useEffect(() => {
    window.addEventListener("resize", callback);
    return () => window.removeEventListener("resize", callback);
  }, [callback]);
};
