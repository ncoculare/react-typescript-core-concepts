import React, { useState } from 'react'

export const DemoControlledForms: React.FC = () => {
  const [value, setValue] = useState('');
  const [valid, setValid] = useState(false);

  const onChange = (e: React.FormEvent<HTMLInputElement>) => {
    setValid(e.currentTarget.value !== '');
    setValue(e.currentTarget.value);
  };

  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    alert(value);
  };

  const onFocus = (e: React.FocusEvent) => {
    setValue('');
    setValid(false);
  };

  return (
    <div className="example-box-centered">
      {
        !valid ? <div className="alert alert-danger">
          FIELD IS REQUIRED
        </div> : null
      }
      <form onSubmit={onSubmit}>
        <input
          className="form-control"
          type="text"
          name="username"
          value={value}
          onChange={onChange}
          onFocus={onFocus}
          placeholder="Write something and press enter"
        />
      </form>

      <hr/>
    </div>
  )
};
