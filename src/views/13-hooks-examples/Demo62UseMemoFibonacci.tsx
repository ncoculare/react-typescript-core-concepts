import React, {useState, useMemo} from "react"

/* --------- USE MEMO

Permette di memorizzare l'ultimo valore calcolato e riutilizzarlo se la sua dipendenza non
è cambiata. Utile per evitare ricalcoli lunghi in rerendering, dove non necessario.

NB: a differenza della memoization (FT technique) solo l'ultimo valore viene cachato
NB2: useMemo != React.memo

  - permette di memorizzare l'ultimo valore computato, da una funzione passata in useMemo
  - gli si passa come seocndo arg una dipendenza

  - nei re rendering successivi, se la dep non è cambiata, usa l'ultimo valore calcolato
 */


const fibonacci = (n: number): number => {
  if (n <= 1) {
    return 1;
  }
  return fibonacci(n - 1) + fibonacci(n - 2);
};

const Demo3UseMemo = () => {
  const [num, setNum] = useState(1);
  const [isGreen, setIsGreen] = useState(true);
  const fib = useMemo(() => fibonacci(num), [num]);     // instead of simply const fib = fibonacci(num)

  return (
    <div>
      <h1
        onClick={() => setIsGreen(!isGreen)}
        style={{ color: isGreen ? "limegreen" : "crimson" }}
      >
        useMemo Example
      </h1>
      <h2>
        Fibonacci of {num} is {fib}
      </h2>
      <button onClick={() => setNum(num + 1)}>+s</button>
    </div>
  );
};

export default Demo3UseMemo;
