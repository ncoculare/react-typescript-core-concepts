import React, { useEffect } from 'react';

interface Product {
  id: number;
}

// component
export const Demo11UseStateCustomHook: React.FC = () => {
  const products = useItems('http://localhost:3001/products')

  return  <h1>
    {products.length} Products
  </h1>
};

// =======================
// custom hook
// =======================
function useItems(url: string) {
  const [items, setItems] = React.useState<Product[]>([]);

  useEffect(() => {
    (async function() {
      const response = await fetch(url, { method: 'GET' });
      setItems(await response.json());
    })();
  },[url]);

  return items;
}
