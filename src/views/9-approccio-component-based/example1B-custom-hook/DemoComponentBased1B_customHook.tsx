import React, { useEffect } from 'react';
import { Form } from '../shared/components/Form';
import { List } from '../shared/components/List';
import { Product } from '../shared/model/product';
import { useFetch } from './useFetch';

const initialActiveState: Product = ({ name: '', price: 0}) as Product;
const initialProductsState: Product[] = [];

// demo with custom hook and useState

export const DemoComponentBased1CustomHook: React.FC = () => {
  const {
    active, products, setActive, http
  } = useFetch(
    'http://localhost:3001/products',
    initialActiveState,
    initialProductsState
  );

  useEffect(() => {
    http.get();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const setActiveHandler = (product: Product) => {
    setActive(product);
  };

  return (
    <>
      <Form
        active={active}
        onAdd={http.save}
        onReset={() => setActive(initialActiveState)}
      />
      <hr/>
      <List
        active={active}
        product={products}
        onDelete={http.delete}
        onSetActive={setActiveHandler}
      />
    </>
  )
};
