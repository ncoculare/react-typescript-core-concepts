import { Product } from '../shared/model/product';
import React, { useState } from 'react';


export function useFetch<T>(
  url: string,
  defaultActiveProduct: Product,
  defaultProducts: Product[]
) {
  const [active, setActive] = useState<Product>(defaultActiveProduct);
  const [products, setProducts] = React.useState<Product[]>(defaultProducts);

  const getHandler = async () => {
    const response = await fetch(url, { method: 'GET' });
    setProducts(await response.json());
  };

  const deleteProductHandler = async (product: Product) => {
    const response = await fetch(`${url}/${product.id}`, { method: 'DELETE' });
    if (response ) {
      await response.json()
    }
    if (response.ok) {
      setProducts(products.filter(u => u.id !== product.id));
      if (product.id === active.id) {
        setActive({...defaultActiveProduct});
      }
    }
  };

  const saveProductHandler = (product: Product) => {
    if (active.id) {
      editProductHandler(product);
    } else {
      addProductHandler(product);
    }
  };

  const addProductHandler = async (product: Product) => {
    const response = await fetch(url, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
      body: JSON.stringify(product)
    });

    if (response.ok) {
      const product = await response.json()
      setProducts([...products, product]);
      setActive({...defaultActiveProduct});
    }
  };

  const editProductHandler = async (product: Product) => {
    const response = await fetch(`${url}/${product.id}`, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
      body: JSON.stringify(product)
    });

    if (response.ok) {
      const newUser = await response.json();
      setProducts(
        products.map(u => {
          return u.id === product.id ? newUser : u;
        })
      );
    }
  };

  return {
    active,
    products,
    setActive,
    setProducts,
    http: {
      get: getHandler,
      delete: deleteProductHandler,
      save: saveProductHandler,
    }
  }
}
