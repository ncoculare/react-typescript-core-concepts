import React  from 'react';
import { Product } from '../shared/model/product';
import { Form } from '../shared/components/Form';
import { List } from '../shared/components/List';
import { useFetch } from './useFetch';

// demo with Custom Hook with useReducer

const initialActiveState: Product = ({ name: '', price: 0}) as Product;
const initialProductsState: Product[] = [];

export const DemoComponentBased2CustomHook: React.FC = () => {

  const {
    active, products, setReset, setActive, http
  } = useFetch(
    'http://localhost:3001/products',
    initialActiveState,
    initialProductsState
  );

  return (
    <>
      <Form
        active={active}
        onAdd={http.save}
        onReset={setReset}
      />
      <hr/>
      <List
        active={active}
        product={products}
        onDelete={http.delete}
        onSetActive={setActive}
      />
    </>
  )
};
